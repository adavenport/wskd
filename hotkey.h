//
// Created by adam on 6/5/19.
//

#ifndef WSKD_HOTKEY_H
#define WSKD_HOTKEY_H


#include <thread>
#include "types.h"

class Hotkey : std::thread {
public:
    void buttonDown();
    void buttonUp();
};

class HotkeyRepeat : Hotkey {
    public:
        string name;
        string button;
    HotkeyRepeat(int delay, string pressed, string sent);
    void buttonDown();
    void buttonUp();
};


#endif //WSKD_HOTKEY_H
