#include "wskd.h"
#include <vector>
#include "tools.h"
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <xcb/xcb_event.h>
#include <signal.h>
#include <xcb/xcb.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int loadConfig(int argc, char **args) {
    /* Default config location should be ~/.config/wskd/wskdrc */
    const string CONFIG = string(std::getenv("HOME")) + "/.config/wskd/wskdrc";
    std::ifstream cfgStream(CONFIG);
//    if (argc > 2){
//        if (((string)args[1]) == "-c"){
//            std::ifstream cfgStream(args[2]);
//        }
//    }
    if (!cfgStream) {
        cfgStream.close();
        return 0;
    }
    string line;
    while (std::getline(cfgStream, line)) {
//        std::cout << line << std::endl;
        if (line[0] != '#')
            parseConfigLine(line);
    }

    cfgStream.close();
    return 1;
}

/* Each argument should be separated by a space */
void parseConfigLine(string &line) {
    auto array = split(line, " ");
    if (array.size() == 3) {
        string &arg = array[0];
        /* Repeat hotkey */
        if (arg == "r:")
            hotkeys[array[1]] = array[2];
            /* Hold hotkey */
        else if (arg == "h:")
            hotkeys[array[1]] = array[2];
    }
}

void keyPress(xcb_generic_event_t *event) {
    xcb_key_press_event_t *keyPressEvent = (xcb_key_press_event_t *) event;
    if (keyPressEvent)
        std::cout << "Key pressed: " << (uint) keyPressEvent->detail << std::endl;

}

void keyRelease(xcb_generic_event_t *event) {
    auto *keyReleaseEvent = (xcb_key_release_event_t *) event;
    if (keyReleaseEvent != nullptr)
        std::cout << "Key released: " << (uint) keyReleaseEvent->detail << std::endl;

}

void grabKey(xcb_keycode_t keycode) {
    uint16_t modfield = 0;
    xcb_generic_error_t *error;
    error = xcb_request_check(connection,
                              xcb_grab_key_checked(connection, true, root, modfield, keycode, XCB_GRAB_MODE_ASYNC,
                                                   XCB_GRAB_MODE_SYNC));
    if (error != nullptr) {
        std::cout << "We have an error" << std::endl;
        free(error);
    } else {
        std::cout << (uint) keycode << " Bound succesfully " << std::endl;
    }
}

void hold(int sig) {
    if (sig == SIGHUP || sig == SIGINT || sig == SIGTERM)
        running = false;
    else if (sig == SIGUSR1)
        reload = true;
    else if (sig == SIGUSR2)
        toggle_grab = true;
    else if (sig == SIGALRM)
        bell = true;
}

int test2(){
    Display*    dpy     = XOpenDisplay(0);
    Window      root    = DefaultRootWindow(dpy);
    XEvent      ev;

    unsigned int    modifiers       = 0;
    int             keycode         = XKeysymToKeycode(dpy,XK_a);
    Window          grab_window     =  root;
    Bool            owner_events    = False;
    int             pointer_mode    = GrabModeAsync;
    int             keyboard_mode   = GrabModeAsync;

    XGrabKey(dpy, keycode, modifiers, grab_window, owner_events,
             pointer_mode,
             keyboard_mode);

    XSelectInput(dpy, root, KeyPressMask);
    while(true)
    {
        XNextEvent(dpy, &ev);
        switch(ev.type)
        {
            case KeyPress:
                std::cout << "Hot key pressed!" << std::endl;
                if (ev.xkey.keycode == XKeysymToKeycode(dpy,XK_Q))
                    return 1;
//                XUngrabKey(dpy,keycode,modifiers,grab_window);
            default:
                break;
        }
    }

    XCloseDisplay(dpy);
    return 0;
}


int setup(){
    Display *display = XOpenDisplay(NULL);
    XFlush(display);
    XEvent event;
    bool done = false;
    XAllowEvents(display, AsyncBoth, CurrentTime);
    uint screen = 0;
    Window root = DefaultRootWindow(display);
    XGrabKey(display, 38, 0, root, true, GrabModeAsync, GrabModeAsync);
    XSelectInput(display, root, KeyPressMask);

    while(!done){
        Window window = XDefaultRootWindow(display);
        XNextEvent(display, &event);
        switch (event.type){
            case KeyPress:
                std::cout << "KeyPress" << std::endl;
            case KeyRelease:
                std::cout << "KeyPress" << std::endl;
        }
    }
}

int main(int argc, char **args) {
    test2();
    if (loadConfig(argc, args)) {
        std::cout << "Config loaded successfully." << std::endl;
        for (const auto &h : hotkeys) {
            std::cout << h.first << " : " << h.second << std::endl;
        }
    } else {
        std::cout << "Error loading config!" << std::endl <<
                  "Either specify a config file or ensure a config file exists under ~/.config/wskd/wskdrc"
                  << std::endl;
        return -1;
    }
    connection = xcb_connect(NULL, NULL);
    xcb_screen_t *screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;

    // Todo disreguard autorepeat
    if (screen == nullptr) {
        std::cout << "Can't connect to screen" << std::endl;
        return -1;
    }
    root = screen->root;

    //Signls from sxhkd that may not be necessary
//    signal(SIGINT, hold);
//    signal(SIGHUP, hold);
//    signal(SIGTERM, hold);
//    signal(SIGUSR1, hold);
//    signal(SIGUSR2, hold);
//    signal(SIGALRM, hold);
    xcb_generic_event_t *event;
    xcb_keycode_t testKey = 38;
    grabKey(testKey);
    uint16_t fd = xcb_get_file_descriptor(connection);
    fd_set descriptors;
    running = true;

    /* Main loop */
    while (running) {
        FD_ZERO(&descriptors);
        FD_SET(fd, &descriptors);
        while ((event = xcb_wait_for_event(connection))) {
//        if (select(fd + 1, &descriptors, NULL, NULL, NULL) > 0) {
//            while ((event = xcb_poll_for_event(connection)) != NULL) {
            uint8_t event_type = XCB_EVENT_RESPONSE_TYPE(event);
            switch (event_type) {
                case XCB_KEY_PRESS:
                    keyPress(event);
                    xcb_allow_events(connection, XCB_ALLOW_SYNC_KEYBOARD, XCB_CURRENT_TIME);
                    xcb_flush(connection);
                    break;
                case XCB_KEY_RELEASE:
                    keyRelease(event);
                    xcb_allow_events(connection, XCB_ALLOW_SYNC_KEYBOARD, XCB_CURRENT_TIME);
                    xcb_flush(connection);
                    break;
//                    case XCB_BUTTON_PRESS:
//                        break;
//                    case XCB_BUTTON_RELEASE:
//                        break;
//                    case XCB_MAPPING_NOTIFY:
//                        std::cout << "Mapping event" << std::endl;
//                        break;
                default:
                    //PRINTF("received event %u\n", event_type);
                    break;
            }
            free(event);
            if (xcb_connection_has_error(connection)) {
                std::cout << "The server closed the connection.\n";
                running = false;
            }
        }
    }

}
