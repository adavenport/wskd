//
// Created by adam on 6/5/19.
//

#ifndef WSKD_WSKD_H
#define WSKD_WSKD_H

#include <xcb/xcb_keysyms.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include "hotkey.h"
#include "types.h"

std::map<string, string> hotkeys;
xcb_connection_t *connection;
xcb_window_t root;
int toggle_grab;
int reload;
int running;
int bell;

int loadConfig(int argc, char** args);
void parseConfigLine(string &line);
void keyPress(xcb_generic_event_t *event);
void keyRelease(xcb_generic_event_t *event);


#endif //WSKD_WSKD_H
