//
// Created by adam on 6/5/19.
//

#ifndef WSKD_TOOLS_H
#define WSKD_TOOLS_H

#include <vector>
#include <string>
#include <string_view>
#include "types.h"

std::vector<std::string> split(const std::string& str, const std::string& delims);

#endif //WSKD_TOOLS_H
